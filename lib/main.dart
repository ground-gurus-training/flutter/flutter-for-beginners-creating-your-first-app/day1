import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    var style = ElevatedButton.styleFrom(
      padding: const EdgeInsets.all(16.0),
      backgroundColor: Colors.green,
    );

    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text(
            'Hello App',
            style: TextStyle(
              fontSize: 32.0,
            ),
          ),
        ),
        body: Container(
          padding: EdgeInsets.all(24.0),
          child: Column(
            children: [
              TextField(
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'Enter your name',
                ),
              ),
              SizedBox(height: 20.0),
              SizedBox(
                width: 160,
                child: ElevatedButton(
                  style: style,
                  onPressed: () => {},
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.person, size: 32.0),
                      Text(
                        'Hello',
                        style: GoogleFonts.nerkoOne(
                          fontSize: 32.0,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 20.0),
              Container(
                color: Colors.yellow,
                padding: EdgeInsets.all(24.0),
                child: Baseline(
                  baseline: 100,
                  baselineType: TextBaseline.ideographic,
                  child: const Text(
                    'Test',
                    style: TextStyle(fontSize: 32),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
